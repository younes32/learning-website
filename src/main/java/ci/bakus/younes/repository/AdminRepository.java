package ci.bakus.younes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.bakus.younes.entities.Admin;

public interface AdminRepository extends JpaRepository<Admin, Long>{

}
