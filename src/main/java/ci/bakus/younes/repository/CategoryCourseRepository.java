package ci.bakus.younes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.bakus.younes.entities.CategoryCourse;

public interface CategoryCourseRepository extends JpaRepository<CategoryCourse, Long>{

}
