package ci.bakus.younes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.bakus.younes.entities.User;

public interface UserRepository extends JpaRepository<User, Long>{

}
