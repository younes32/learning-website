package ci.bakus.younes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.bakus.younes.entities.Course;

public interface CourseRepository extends JpaRepository<Course, Long>{

}
