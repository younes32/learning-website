package ci.bakus.younes.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ci.bakus.younes.entities.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long>{

}
