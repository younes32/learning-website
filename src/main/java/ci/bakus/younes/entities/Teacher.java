package ci.bakus.younes.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Teacher implements Serializable{

	@Id
	@GeneratedValue
	private Long idTeacher;
	private String name;
	private String surname;
	private String speciality;
	
	public Teacher() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Teacher(Long idTeacher, String name, String surname, String speciality) {
		super();
		this.idTeacher = idTeacher;
		this.name = name;
		this.surname = surname;
		this.speciality = speciality;
	}

	public Long getIdTeacher() {
		return idTeacher;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getSpeciality() {
		return speciality;
	}

	public void setSpeciality(String speciality) {
		this.speciality = speciality;
	}
	
	
}
