package ci.bakus.younes.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Course implements Serializable{
	
	@Id
	@GeneratedValue
	private Long idCourse;
	private String flagCourse;
	@ManyToOne
	@JoinColumn(name="CATEGORY_COURSE")
	private CategoryCourse categoryCourse;
	
	public Course() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Course(String flagCourse) {
		super();
		this.flagCourse = flagCourse;
	}

	public Long getIdCourse() {
		return idCourse;
	}
	public void setIdCourse(Long idCourse) {
		this.idCourse = idCourse;
	}
	public String getFlagCourse() {
		return flagCourse;
	}
	public void setFlagCourse(String flagCourse) {
		this.flagCourse = flagCourse;
	}
    
	
}
