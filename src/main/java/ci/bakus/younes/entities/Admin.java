package ci.bakus.younes.entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Admin implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idAdmin;
	private String login;
	private String password;

	public Admin() {
		super();
		// TODO Auto-generated constructor stub
	}
    
	
	public Admin(Long idAdmin, String login, String password) {
		super();
		this.idAdmin = idAdmin;
		this.login = login;
		this.password = password;
	}


	public Long getIdAdmin() {
		return idAdmin;
	}


	public void setLogin(String login) {
		this.login = login;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public String getLogin() {
		return login;
	}

	public String getPassword() {
		return password;
	}

}
