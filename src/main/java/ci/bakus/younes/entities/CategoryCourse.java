package ci.bakus.younes.entities;

import java.io.Serializable;
import java.util.Collection;


import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class CategoryCourse implements Serializable{
    
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idCategorieCourse;
	private String flagCategorieCourse;
	@OneToMany(mappedBy = "Course",fetch = FetchType.LAZY)
	Collection<Course> courses;
	
	public CategoryCourse() {
		super();
		// TODO Auto-generated constructor stub
	}

	public CategoryCourse(String flagCategorieCourse) {
		super();
		this.flagCategorieCourse = flagCategorieCourse;
	}

	public Long getIdCategorieCourse() {
		return idCategorieCourse;
	}

	public void setIdCategorieCourse(Long idCategorieCourse) {
		this.idCategorieCourse = idCategorieCourse;
	}

	public String getFlagCategorieCourse() {
		return flagCategorieCourse;
	}

	public void setFlagCategorieCourse(String flagCategorieCourse) {
		this.flagCategorieCourse = flagCategorieCourse;
	}
	
}
