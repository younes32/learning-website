package ci.bakus.younes.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class User implements Serializable {

	@Id
	@GeneratedValue
	private Long idUser;
	private String name;
	private String surname;
	private Date birthday;
	private String login;
	private String password;
    
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
  
	
	public User(String name, String surname, Date birthday, String login, String password) {
		super();
		this.name = name;
		this.surname = surname;
		this.birthday = birthday;
		this.login = login;
		this.password = password;
	}

	public Long getIdUser() {
		return idUser;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
