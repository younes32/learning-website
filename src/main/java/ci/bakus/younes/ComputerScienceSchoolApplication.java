package ci.bakus.younes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ComputerScienceSchoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(ComputerScienceSchoolApplication.class, args);
	}

}
